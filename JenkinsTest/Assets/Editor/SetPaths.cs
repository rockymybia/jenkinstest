﻿using UnityEditor;
using UnityEngine;

public class SetPaths : MonoBehaviour {

    //[MenuItem("Build/SetNDKPath")]
    public static void SetNDKPath()
    {
        EditorPrefs.SetString("AndroidNdkRoot", @"D:\android-ndk-r13b-windows-x86_64");
        Debug.LogFormat("Android NDK Path is now {0}", EditorPrefs.GetString("AndroidNdkRoot"));
    }
}
